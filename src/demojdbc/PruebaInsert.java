
package demojdbc;

import java.sql.*;

public class PruebaInsert {
    public static void main(String[] args) throws SQLException {
        String nombreBD = "prueba";
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/" + nombreBD, "root", "");
        
        Statement stmt = conn.createStatement();
        String sql = "INSERT INTO BOLETA VALUES (NULL, '2018-03-14', 'X')";        
        stmt.executeUpdate(sql);
        System.out.println("Inserción exitosa!");
        
        conn.close();
    }
}
