
package demojdbc;

import java.sql.*;

public class PruebaSelect {
    public static void main(String[] args) throws SQLException {
        String nombreBD = "prueba";
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/" + nombreBD, "root", "");
        
        Statement stmt = conn.createStatement();
        String sql = "SELECT * FROM BOLETA";        
        ResultSet rs = stmt.executeQuery(sql);
        int numReg = 1;
        
        while(rs.next()) {
            System.out.println("Boleta " + numReg + ":");            
            System.out.println("ID: " + rs.getInt("id"));
            System.out.println("Fecha: " + rs.getDate("fecha"));
            System.out.println("Titular: " + rs.getNString("titular"));
            System.out.println();
        }
        
        conn.close();
    }
}
