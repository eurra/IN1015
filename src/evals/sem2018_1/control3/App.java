
package evals.sem2018_1.control3;

public class App {
    public static void main(String[] args) {
        // Caso 1
        CompraOnline c1 = new CompraOnline(1234, "polera", 20);
        c1.agregarUnidadesCompra(10);
        System.out.println("Costo compra 1: " + c1.obtenerCostoCompra());
        
        // Caso 2
        CompraOnline c2 = new CompraOnline(5678, "pantalon", 30);
        c2.agregarUnidadesCompra(5);
        c2.cancelarCompra();
    }
}
