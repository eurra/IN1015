
package evals.sem2018_1.control3;

public class CompraOnline {
    private int ID;
    private String nombreProd;
    private int precioUnidad;
    private int cantComprado;

    public CompraOnline(int ID, String nombreProd, int precioUnidad) {
        this.ID = ID;
        this.nombreProd = nombreProd;
        this.precioUnidad = precioUnidad;
    }
    
    public void agregarUnidadesCompra(int cant) {
        cantComprado += cant;
    }
    
    public int obtenerCostoCompra() {
        return precioUnidad * cantComprado;
    }
    
    public boolean cancelarCompra() {
        if(cantComprado > 0) {
            cantComprado = 0;
            return true;
        }
        
        return false;
    }
}
