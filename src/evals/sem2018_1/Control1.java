
package evals.sem2018_1;

import java.util.Scanner;

public class Control1 {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        System.out.println("Ingrese tabla a mostrar");
        int tabla = lector.nextInt();
        int suma = 0;
        
        for(int i = 1; i <= 12; i++) {
            int mult = i * tabla;
            System.out.println(tabla + " x " + i + " = " + mult);
            suma += mult;
        }
        
        if(suma > 100)
            System.out.println("La suma de las multiplicaciones es mayor a 100.");
    }
}
