
package evals.sem2018_1;

import java.util.Scanner;

public class Control2 {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        System.out.println("Ingrese max. camillas");
        int N = lector.nextInt();
        String camillas[] = new String[N];
        boolean usos[][] = new boolean[N][30];
        boolean continuar = true;
        
        do {
            System.out.println("Seleccione opción:");
            System.out.println("1 - Agregar nueva camilla");
            System.out.println("2 - Ocupar camilla en día específico");
            System.out.println("3 - Calcular grado ocupación en día");
            System.out.println("4 - Salir");
            int opc = lector.nextInt();
            
            switch(opc) {
                case 1: {
                    System.out.println("Ingrese nombre de camilla");
                    String nom = lector.next();
                    boolean repetido = false;
                    int posDisponible = -1;
                    
                    for(int i = 0; i < camillas.length; i++) {
                        if(camillas[i] == null)
                            posDisponible = i;
                        else if(camillas[i].equals(nom))
                            repetido = true;
                    }
                    
                    if(repetido) {
                        System.out.println("Nombre de camilla ya existe");
                    }
                    else if(posDisponible == -1) {
                        System.out.println("No hay espacio para agregar la camilla");
                    }
                    else {
                        camillas[posDisponible] = nom;
                        System.out.println("Camilla agregada exitosamente!");
                    }
                    
                    break;
                }
                case 2: {
                    System.out.println("Ingrese nombre de camilla");
                    String nom = lector.next();
                    System.out.println("Ingrese día del mes");
                    int dia = lector.nextInt();
                    boolean encontrado = false;
                    
                    for(int i = 0; i < camillas.length && !encontrado; i++) {
                        if(camillas[i] != null && camillas[i].equals(nom)) {
                            encontrado = true;
                            usos[i][dia] = true;
                            System.out.println("Camilla asignada a día!");
                        }
                    }
                    
                    break;
                }
                case 3: {
                    System.out.println("Ingrese día del mes");
                    int dia = lector.nextInt();
                    double totalCamillas = 0;
                    int totalCamillasOcupadas = 0;
                    
                    for(int i = 0; i < camillas.length; i++) {
                        if(camillas[i] != null) {
                            totalCamillas++;
                            
                            if(usos[i][dia] == true)
                                totalCamillasOcupadas++;
                        }
                    }
                    
                    if(totalCamillas == 0)
                        System.out.println("No hay camillas en el sistema, noe s posible calcular");
                    else
                        System.out.println("El porcentaje de camillas ocupadas en el dia es " + ((totalCamillasOcupadas / totalCamillas) * 100));
                    
                    break;
                }
                case 4: {
                    continuar = false;
                }
            }
        }
        while(continuar);
    }
}
